// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBnxcRB2TSS0Znvfzm9WUl35FoF512XnhQ",
    authDomain: "testing-28866.firebaseapp.com",
    databaseURL: "https://testing-28866.firebaseio.com",
    projectId: "testing-28866",
    storageBucket: "testing-28866.appspot.com",
    messagingSenderId: "9284271261",
    appId: "1:9284271261:web:ea72ef8169d8b8e283a874",
    measurementId: "G-VRQ75TPZKG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
