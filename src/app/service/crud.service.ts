import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { EmpList } from '../emp.interface';


@Injectable({
  providedIn: 'root'
})
export class CrudService {
  constructor(public fireser:AngularFirestore) { 
  }

  createemprec(record){
    return this.fireser.collection('Employee').add(record);
  }
 getAllEmpRec(){
   return this.fireser.collection('Employee').snapshotChanges();
 }
 updaterecord(id,record){
return this.fireser.doc('Employee/'+id).update(record);
 }
 deleterec(recid){
   return this.fireser.doc('Employee/'+recid).delete();
 }
}
