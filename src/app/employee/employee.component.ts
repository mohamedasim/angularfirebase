import { Component, OnInit } from '@angular/core';
import { CrudService } from '../service/crud.service';
import { EmpList, Detail } from '../emp.interface';
import { Router } from '@angular/router';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  titlestring="";
  empdetail:EmpList[];
  detail:Detail[];
  employeeName:string;
  detailemp=false;
  detailemp1=true;
  ondetail=false;
  onedit=false;
  employee:any;
  employeeId:number;
  employeeAge:number;
  isedit=false;
  oneditshow=false;
  detailshow=false;
  employeeAddress:string;
  message:string;
  detailname="a";
  detailage:number;
  detailaddress:string;
  constructor(public service:CrudService,public router:Router){}
  ngOnInit(): void {
    this.service.getAllEmpRec().subscribe(data=>{
      this.employee = data.map(e=>{
        return{
          id:e.payload.doc.id,
          isedit:false,
          name: e.payload.doc.data()['name'],
          age: e.payload.doc.data()['age'],
          address: e.payload.doc.data()['address'],
        };
      })
    })
  }
  resetform(){
    this.titlestring="add record";
    this.employeeName="";
    this.employeeAge=undefined;
    this.employeeAddress="";
    this.onedit=true;
    this.detailshow=false;
    this.message="";
  }
  id:any;
editrec(record){
  this.titlestring="edit record";
  this.onedit=false;
  this.oneditshow=true;
  this.detailshow=false;
  this.id=record.id;
  // record.id = record.id
record.editname = record.name;
record.editage = record.age;
record.editaddress = record.address;
if(this.isedit=true){
  this.employeeName=record.editname;
  this.employeeAge=record.editage;
  this.employeeAddress=record.editaddress;
}
//return this.isedit=false;
//this.editname = record.detailname
//console.log(this.detailname);
  }
  updateemp(){
    alert('updated')
let record ={};
record['name']= this.employeeName;
record['age']= this.employeeAge;
record['address']= this.employeeAddress;
console.log(record['name']);
//record['age']=recorddata.editage;
//record['address']=recorddata.editaddress;
this.service.updaterecord(this.id,record);
//recorddata.isedit=false;
  }
  deleterec(recid){
    this.service.deleterec(recid);
  }
  details(rec){
    this.titlestring="details";
    this.oneditshow=false;
    this.isedit=false;
    this.onedit=false;
    this.detailshow=true;
  this.detailname=rec.name;
  this.detailage=rec.age;
  this.detailaddress=rec.address;
  console.log(this.detailname);
}
  createemp(){this.isedit=false;this.detailshow=false;
    alert('form is submitted.');
    let record={};
    // record['id']=this.employeeId;
    record['name']=this.employeeName;
    record['age']=this.employeeAge;
    record['address']=this.employeeAddress;
    this.service.createemprec(record).then(res =>{
this.employeeName="";
this.employeeAge= undefined;
this.employeeAddress="";
console.log(res);
this.message="Employee data saved";
    }).catch(error =>{
      console.log(error);
      
    });
  }
}
