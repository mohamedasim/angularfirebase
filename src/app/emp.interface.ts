export interface EmpList {
     id:number,
    name:string,
    age:number,
    address:string,
    isedit:false,
    editname:string,
    editage:number,
    editaddress:string
}

export interface Detail{
    detailname:string,
    detailage:number,
    detailaddress:string
}