import { Component,OnInit } from '@angular/core';
import { CrudService } from './service/crud.service';
import { EmpList, Detail } from './emp.interface';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import {AngularFirestore} from '@angular/fire/firestore/angular-fire-firestore'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angfirebase';

}
